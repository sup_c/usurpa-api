import sys

import config, ezodf
from models import RegionalZone, UsurpaSection, Squat
import lxml.etree as etree
from ezodf.styles import OfficeAutomaticStyles

CELL_WIDTH=8 # number of cells to iterate when analize a row
appName = config.APP_BASE_NAME
usurpaOds = config.USURPA_DOWNLOAD_PATH
sectionList = {}
squatList = {}
days = [] # Used to store the days


class AutomaticStyles:

    _TAG = "{urn:oasis:names:tc:opendocument:xmlns:office:1.0}automatic-styles"

    _PROP_PREFIX='{urn:oasis:names:tc:opendocument:xmlns:style:1.0}'
    TABLE_CELL_PROPERTIES = _PROP_PREFIX+"table-cell-properties"
    PARAGRAPH_PROPERTIES = _PROP_PREFIX+"paragraph-properties"
    TEXT_PROPERTIES = _PROP_PREFIX+"text-properties style"

    def __init__(self, xmlnode):
        xmlstyles = next((child for child in xmlnode if child.tag == self._TAG), None)
        self.automaticStyles = OfficeAutomaticStyles(xmlstyles)

    def getStyleProperties(self, styleName, prop):
        xmlstyle = self.automaticStyles._find(styleName)
        if xmlstyle is None: return None
        return next((child for child in xmlstyle if child.tag == prop ), None)

    def getBackgroundColor(self, styleName):
        tag = '{urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0}background-color'
        cellStyle = self.getStyleProperties(styleName, AutomaticStyles.TABLE_CELL_PROPERTIES)
        return cellStyle.get(tag) if cellStyle is not None else None



def parse(input=usurpaOds):

    print("# Parsig", input)

    doc = ezodf.opendoc(input)
    if doc.doctype == 'odt':

        # Get styles
        print("* Parsing styles")
        styles = AutomaticStyles(doc.filemanager.get_xml_element('content.xml'))

        print("* Parsing sections")
        # sheet = doc.sheets[0]
        # sheet = doc.body.filter(kind='Table')
        # print(doc.body.__dict__)
        sheet = None
        for obj in doc.body:
            if type(obj) == ezodf.table.Table:
                sheet = obj
                break

        section = None
        for rindex, row in enumerate(sheet.rows()):
            stylename = row[0].style_name
            bgColor = styles.getBackgroundColor(stylename)
            print(row[0].value)

            # If bgColor is black mark the beginning of a new UsurpaSection
            if bgColor == "#000000":
                # Store old section
                if section is not None: sectionList[section.title] = section
                section = UsurpaSection(row[0].value)
                actualSection = len(sectionList)

            # Now lets analize the entire row
            if section is not None:
                # todo: First section. Demonstrations, info usurpa number and week
                if actualSection == 0:
                    pass
                # todo: "Jornades i actes ocasionals"
                elif actualSection == 1:
                    # The first cell is the title and the second is spanned until the end with the event info
                    pass
                # Days and their position
                elif actualSection == 2:
                    for i in range(1, CELL_WIDTH):
                        days.append(row[i].value)
                # Squats
                elif actualSection > 2:
                    # If is not the header of the section
                    if bgColor != "#000000":
                        # First create the Squat object... Oh yeah
                        if row[0].value is None: continue
                        squat = Squat(row)
                        # Then parse the day events
                        for i in range(1, CELL_WIDTH):
                            # todo: fix the template because there are two spanned cells that are not visible
                            print(days)
                            day = i-1 # The day of the week (this loop starts at 1 and the array of days inside squat at 0)
                            print(days[day])
                            print(row[i].value)
                            print(row[i].span)

                            if squat.events[day] is None:
                                # If the cell is spanned to more than one days copy the event on this days
                                for span in range(0, row[i].span[1]): squat.events[day + span] = row[i].value
                        # Check if has aditional info
                        if squat.hasRowSpan:
                            for i in range(1, squat.rowSpan):
                                squat.additionalInfo.append(sheet.row(rindex+1)[1].value)
                        squatList[squat.name] = squat

                section.addRow(row)

        print("* Squats found:", len(squatList))

        # for k,v in sectionList.items():
        #     print(k)

    else:
        print("Bad format for", input)
        raise


    d = 2
    s = "AT. LLIB. GRÀCIA"
    print(squatList[s].name, ": ", days[d], squatList[s].events[d])
    print("INFO adicional: "+"".join(squatList[s].additionalInfo if squatList[s].hasRowSpan else "No te informacio adicional"))

    d=3
    s="HORT VALLCARCA"
    print(squatList[s].name, ": ", days[d], squatList[s].events[d])
    print("INFO adicional: "+"".join(squatList[s].additionalInfo if squatList[s].hasRowSpan else "No te informacio adicional"))
