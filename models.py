import config

# squatNameExceptions = config.NAME_EXCEPTIONS
# zones = config.REGIONAL_ZONES

class UsurpaSection(object):
    def __init__(self, title):
        self.title = title
        self.rows = []

    def addRow(self, row):
        self.rows.append(row)


class Squat(object):
    def __init__(self, row):
        self.row = row
        # The ezodf cell object should have 3 lines corresponding to
        # 1. Squat name
        # 2. Address
        # 3. Metro station
        cellSplit = row[0].value.split("\n")
        self.name = cellSplit[0] if 0 < len(cellSplit) else None
        self.address = cellSplit[1] if 1 < len(cellSplit) else None
        self.metro = cellSplit[2] if 2 < len(cellSplit) else None

        # Check if has extra information (a row split)
        self.hasRowSpan = True if row[0].span[0] > 1 else False
        self.rowSpan = row[0].span[0]
        self.additionalInfo = []

        # Initialize th 7 days of the week
        self.events = [None] * 7


class RegionalZone (object):

    def __init__(self, name=None):
        self.name = name
        self.rows = []
        self.squatList = []

    def addRow(self, row):
        """
        Add a row object [] into the sets of self.rows[]
        :param row:
        :return:
        """
        self.rows.append(row)

    # TODO: DRY this function is basically the same as parser.parseCSV
    def generateSquats(self):
        squat = None
        for row in self.rows:
            squatName = Squat.isSquatName(row[0])
            if squatName:
                squat = Squat(squatName)
                self.squatList.append(squat)
            if squat is not None:
                squat.addRow(row)

    # TODO: Try to do this without making loop
    # Check if a row contains a zone name defined on config file
    @classmethod
    def checkIfIsZone(cls, row):
        for zone in zones:
            if zone in row:
                return zone
        return False


class GancioEvent:

    def __init__(self,title,description,place_name,place_address,start_datetime,end_datetime):
        self.title  = str(title)
        self.description = str(description)
        self.place_name = str(place_name)
        self.place_address = str(place_address)
        self.start_datetime = int(start_datetime)
        self.end_datetime = int(end_datetime)
        self.multidate = "False"

    def getJson(self):
        EventDict = {}
        EventDict["title"] = self.title
        EventDict["description"] = self.description
        EventDict["place_name"] = self.place_name
        EventDict["place_address"] = self.place_address
        EventDict["start_datetime"] = self.start_datetime
        EventDict["multidate"] = self.multidate
        return EventDict
    
    def getMultipartFormData(self):
        body = {}
        for atribute in self.__dict__.keys():
            body[atribute] = (None, self.__dict__[atribute])
        return body