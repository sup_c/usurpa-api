#!/usr/bin/python

# Name of the application
APP_BASE_NAME = 'usurpa-api'

# File to update
USURPA_URL = 'https://usurpa.squat.net/usurpa.pdf'
USURPA_DOWNLOAD_PATH = 'usurpa-proves1.ods'

# Gancio
GANCIO_URL = "http://localhost:13120"
# TODO: Seguro que hay una forma mas segura de hacer esto, pero lo mirare mas adelante
GUSER = "example@example.es"
GPASSWORD = "examplepassword"
