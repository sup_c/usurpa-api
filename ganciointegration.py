#!/usr/bin/env python3

from models import GancioEvent
import requests
import config

# Hace una petición POST a /api/event con los datos de evento
def CreateEvent(sendData,token):
    try:
        headers = {'Authorization': 'Bearer ' + token }
        url = config.GANCIO_URL + "/api/event"
        resp = requests.post(url,files = sendData,headers = headers)
        if resp.status_code == 200:
            print("Se ha creado el evento con exito")
    except Exception as e:
        print("Error doing the request to the api:")
        print(str(e))

def GetLoginToken():
    url = config.GANCIO_URL
    payload = {"username" : config.GUSER , "password" : config.GPASSWORD , "grant_type": "password" , "client_id": "self"}
    resp = requests.post(url,data= payload)
    return resp.json()["access_token"]

def UploadEventGancio(Event):
    try:
        event = GancioEvent(Event["title"],Event["description"],Event["squat"],Event["street"],Event["start_datetime"],Event["end_datetime"],"False")
        token = GetLoginToken()
        sendData = event.getMultipartFormData()
        CreateEvent(sendData,token)
    except Exception as e:
        print("Error creating the event in gancio")
        print(str(e))
    return


#GancioEvent = {"title": "" , "description", "placename": "", "placeaddress": "", "startdatetime", "", "end_datetime": ""  }